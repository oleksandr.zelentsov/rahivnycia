from django.contrib import admin

from debts.models import CalculationResult, Graph, BinaryFile, UserSettings, IPTimezonePair

admin.site.register(CalculationResult)
admin.site.register(Graph)
admin.site.register(BinaryFile)
admin.site.register(UserSettings)
admin.site.register(IPTimezonePair)