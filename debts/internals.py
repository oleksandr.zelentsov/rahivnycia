import copy
import logging
from csv import DictReader
from functools import partial
from io import BytesIO
from numbers import Number
from os import unlink
from typing import NamedTuple, Mapping, List, Sequence, Iterable, Optional

import networkx as nx
import pytz
import requests
from commonlib import function_composition, attach_appropriate_names_for_fields
from django.utils.translation import gettext, ngettext
from matplotlib import pyplot as plt

from rah.settings import IP_STACK_ACCESS_KEY

PLOT_FILENAME_FORMAT = '{plot_name}-{date:%Y-%m-%d}.{extension}'

currency_round = partial(round, ndigits=2)
get_money = function_composition(currency_round, float)


class Calculations(NamedTuple):
    sum_total: Number
    total: Number
    people: Mapping


class WhoOwesWho(NamedTuple):
    who_owes: str
    who_is_owed: str
    debt: Number

    @classmethod
    def from_debt_graph(cls, graph: nx.DiGraph):
        who_owes_who = []
        for person1, creditors in graph.adj.items():
            for person2, attributes in creditors.items():
                who_owes_who.append(
                    cls(
                        who_owes=person1,
                        who_is_owed=person2,
                        debt=get_money(attributes.get('debt'))
                    )
                )

        return who_owes_who

    def __str__(self):
        owes = gettext('owes')
        return f'{self.who_owes} {owes} {self.who_is_owed} {self.debt:.2f}'


class PaymentHistoryItem:
    def __init__(self, total_price, people, who_paid, **metadata):
        self._total_price = get_money(total_price)
        self._who_paid = who_paid
        if isinstance(people, dict):
            self._people = people
        elif isinstance(people, Sequence):
            if len(people) < 2:
                raise ValueError('there is no point of calculating 0 or 1 payment debts')
            self._people = {person: 1 for person in people}
        else:
            raise TypeError('wrong type of "people" argument')

        self.payment_meta = metadata

    @property
    def total_price(self):
        return self._total_price

    @property
    def people(self):
        return self._people

    @property
    def who_paid(self):
        return self._who_paid

    def __dict__(self):
        return dict(total_price=self.total_price, people=self.people, **self.payment_meta)

    def __repr__(self):
        meta = ('' if len(self.payment_meta) == 0 else ', ' + ', '.join(
            ['{k}={v}'.format(k=key, v=repr(value)) for key, value in self.payment_meta.items()]))
        return f'{self.__class__.__qualname__}({self.total_price}, {repr(self.people)}{meta})'

    def __str__(self):
        effective_people_count = len(list(filter(lambda k: self.people[k] != 0, self.people)))
        paid_ = gettext('paid')
        divided_between_ = ngettext('for', 'divided between', effective_people_count)
        people_ = ngettext('person', 'people', effective_people_count)
        return f'{self.who_paid} {paid_} {self.total_price}, {divided_between_} {effective_people_count} {people_}'

    def calculate(self):
        total_count = sum(self.people.values())
        one_price = get_money(self.total_price / total_count)
        result = {}
        for person, count in self.people.items():
            person_price = get_money(count * one_price)
            if person_price != 0:
                result[person] = person_price

        return result

    @classmethod
    def from_dict(cls, dict_):
        return cls(
            total_price=dict_.get('total_price'),
            people=dict_.get('people'),
            who_paid=dict_.get('who_paid'),
        )

    @classmethod
    def get_people_involved(cls, histories: 'List[PaymentHistoryItem]'):
        return {person for history in histories for person in history.people}

    @classmethod
    def aggregate_calculations(cls, histories: 'Sequence[PaymentHistoryItem]', who_paid: str = None) -> Calculations:
        if who_paid is not None:
            histories = filter(lambda x: x.who_paid == who_paid, histories)

        calculated_histories = map(cls.calculate, histories)
        people_to_their_money = {}
        for history in calculated_histories:
            for person, money in history.items():
                people_to_their_money.setdefault(person, 0)
                people_to_their_money[person] += get_money(money)

        people_to_their_money = {
            person: get_money(money)
            for person, money in people_to_their_money.items()
        }
        total_price_of_all_transactions = sum([history.total_price for history in histories])
        sum_total_of_all_debts = sum([money for person, money in people_to_their_money.items()])
        return Calculations(
            sum_total=sum_total_of_all_debts,
            total=total_price_of_all_transactions,
            people=people_to_their_money,
        )

    @classmethod
    def _from_csv_row(cls, row: dict) -> 'Optional[PaymentHistoryItem]':
        try:
            total_price = get_money(row.pop('total_price'))
            who_paid = row.pop('who_paid')
        except (TypeError, ValueError):
            return None
        except KeyError:
            raise ValueError('file you submitted is incorrectly formatted')

        metadata_fields = [field for field in row.keys() if field.startswith(':')]
        metadata = {key.strip(':'): row.pop(key) for key in metadata_fields}
        try:
            people = {field: get_money(row[field]) for field in row}
        except (TypeError, ValueError):
            return None

        return cls(total_price=total_price, people=people, who_paid=who_paid, **metadata)

    @classmethod
    def from_csv(cls, filename: str = None, fp=None, **parser_args) -> 'List[PaymentHistoryItem]':
        if filename is not None and fp is None:
            with open(filename, 'r') as fp:
                reader = DictReader(fp, **parser_args)
                rows = [dict(row) for row in reader]
        elif filename is None and fp is not None:
            reader = DictReader(fp, **parser_args)
            rows = [dict(row) for row in reader]
        else:
            raise ValueError('only one of the arguments should be filled')

        rows = attach_appropriate_names_for_fields(rows)
        res = []
        for row in rows:
            v = cls._from_csv_row(row)
            if v is not None:
                res.append(v)

        return res

    @classmethod
    def get_who_owes_who(cls, histories: 'Sequence[PaymentHistoryItem]',
                         all_people: 'Iterable[str]') -> List[WhoOwesWho]:
        people_calculations = {}
        for person in all_people:
            people_calculations[person] = cls.aggregate_calculations(histories, who_paid=person)

        who_owes_who = []
        unique_pairs = set([frozenset([p1, p2]) for p1 in all_people for p2 in all_people if p1 != p2])
        for person1, person2 in unique_pairs:
            p1_to_p2 = people_calculations[person2].people.get(person1, 0)
            p2_to_p1 = people_calculations[person1].people.get(person2, 0)
            second_one_owes_first_one = sorted(((p2_to_p1, person1), (p1_to_p2, person2)), key=lambda x: x[0])
            (how_much1, who_owes), (how_much2, who_is_owed) = second_one_owes_first_one
            if how_much1 == how_much2:
                continue

            how_much2 -= how_much1
            how_much2 = get_money(how_much2)
            who_owes_who.append(WhoOwesWho(debt=how_much2, who_owes=who_owes, who_is_owed=who_is_owed))

        return who_owes_who

    @classmethod
    def minimize_debt_graph(cls, complex_graph: nx.DiGraph) -> nx.DiGraph:
        graph = copy.deepcopy(complex_graph)
        work_done = True  # just a setup
        while work_done:
            work_done = False
            for p1, p2, d12 in graph.edges(data='debt', default=0):
                for _, p3, d23 in graph.edges([p2], data='debt', default=0):
                    points_are_not_unique = p1 == p2 or p2 == p3 or p1 == p3
                    is_wrong_condition = all([d == 0 for d in [d12, d23]]) or points_are_not_unique
                    if is_wrong_condition:
                        continue

                    if d12 == d23:
                        logging.debug(f'(1) simplifying {p1} -> {p2} -> {p3} to {p1} -> {p3}')
                        cls._method1(d12, graph, p1, p2, p3)
                        work_done = True
                    elif d12 > d23:
                        logging.debug(f'(2) simplifying {p1} -> {p2} -> {p3} to {p1} -> {p2}, {p1} -> {p3}')
                        cls._method2(d12, d23, graph, p1, p2, p3)
                        work_done = True
                    elif d12 < d23:
                        logging.debug(f'(3) simplifying {p1} -> {p2} -> {p3} to {p1} -> {p3}, {p2} -> {p3}')
                        cls._method3(d12, d23, graph, p1, p2, p3)
                        work_done = True

                    if work_done:
                        break
                if work_done:
                    break

        assert are_graphs_equivalent(complex_graph, graph)

        return graph

    @classmethod
    def _method1(cls, d12, graph, p1, p2, p3):
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = get_money(d12 + graph.edges[p1, p3]['debt'])
            graph.remove_edge(p1, p3)
        else:
            new_debt = d12

        graph.add_edge(p1, p3, debt=new_debt)

    @classmethod
    def _method2(cls, d12, d23, graph, p1, p2, p3):
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = get_money(d23 + graph.edges[p1, p3]['debt'])
            graph.remove_edge(p1, p3)
        else:
            new_debt = d23

        graph.add_edge(p1, p2, debt=get_money(d12 - d23))
        graph.add_edge(p1, p3, debt=new_debt)

    @classmethod
    def _method3(cls, d12, d23, graph, p1, p2, p3):
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = get_money(d12 + graph.edges[p1, p3]['debt'])
            graph.remove_edge(p1, p3)
        else:
            new_debt = d12

        graph.add_edge(p1, p3, debt=new_debt)
        graph.add_edge(p2, p3, debt=get_money(d23 - d12))

    @classmethod
    def build_debt_graph(cls,
                         all_people: Sequence,
                         histories: 'Sequence[PaymentHistoryItem]',
                         who_owes_who: Sequence[WhoOwesWho] = None) -> nx.DiGraph:
        if who_owes_who is None:
            who_owes_who = cls.get_who_owes_who(histories, all_people)

        graph = nx.DiGraph()
        for person in all_people:
            graph.add_node(person)

        for w in who_owes_who:
            graph.add_edge(w.who_owes, w.who_is_owed, debt=w.debt)

        return graph


def draw_digraph(graph: nx.DiGraph, full_names: bool = False, output_filename: str = None, file_extension=None, return_file_content=False):
    edge_labels = dict([  # todo simplify
        (
            (u, v,), d['debt']
        )
        for u, v, d in graph.edges(data=True)
    ])
    positioning = nx.spring_layout(graph, k=10, random_state=1)
    nx.draw_networkx_nodes(graph, positioning, cmap=plt.get_cmap('jet'), node_size=500)
    nx.draw_networkx_edge_labels(graph, positioning, edge_labels=edge_labels)
    if not full_names:
        display_labels = {
            person: ''.join([v[0] for v in person.split()]).upper()
            for person in graph.nodes
        }
        nx.draw_networkx_labels(graph, positioning, labels=display_labels)
    else:
        nx.draw_networkx_labels(graph, positioning)

    nx.draw_networkx_edges(graph, positioning, arrows=True, arrowsize=25)
    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename, format=file_extension, bbox_inches='tight')
        plt.close()
        if return_file_content:
            s = BytesIO()
            with open(output_filename, 'rb') as fp:
                s.write(fp.read())

            unlink(output_filename)
            return s.getvalue().decode()


class TimezoneLookupError(Exception):
    pass


def get_timezone_by_request(request):
    from ipware import get_client_ip
    from debts.models import IPTimezonePair

    client_ip, routable = get_client_ip(request)
    if not routable:
        raise TimezoneLookupError('non-routable IP passed')

    if request.user.is_authenticated:
        try:
            pair = IPTimezonePair.objects.get(user=request.user)
        except IPTimezonePair.DoesNotExist:
            pass
        else:
            return pair.timezone_name

    parameters = dict(
        access_key=IP_STACK_ACCESS_KEY,
        fields='all,location',
    )
    freegeoip_response = requests.get(
        f'http://api.ipstack.com/{client_ip}',
        params=parameters,
    )
    if freegeoip_response.status_code != 200:
        raise TimezoneLookupError(f'IP Stack request error: {freegeoip_response.content}')

    freegeoip_response_json = freegeoip_response.json()
    zone, city = freegeoip_response_json['continent_name'], freegeoip_response_json['location']['capital']
    tz = f'{zone}/{city}'
    if tz not in pytz.common_timezones:
        raise TimezoneLookupError(f'{tz} is not in the pytz.timezones list')

    if request.user.is_authenticated:
        data_to_update = dict(timezone_name=tz)
        IPTimezonePair.objects.update_or_create(defaults=data_to_update, user=request.user, **data_to_update)

    return tz


def are_graphs_equivalent(graph_complex, graph_simplified):
    complex_graph_integrity = get_graph_integrity(graph_complex)
    simplified_graph_integrity = get_graph_integrity(graph_simplified)

    return complex_graph_integrity == simplified_graph_integrity


def get_graph_integrity(graph: nx.DiGraph):
    integrity = {}

    for person in graph.nodes:
        integrity.setdefault(person, 0)

    for p1, p2, debt in graph.edges.data('debt', default=0):
        integrity[p1] = get_money(integrity[p1] - debt)
        integrity[p2] = get_money(integrity[p2] + debt)

    return integrity