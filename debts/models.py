import hashlib
from datetime import datetime
from io import StringIO, BytesIO
from os import path

import pytz
from django.contrib.auth.models import User
from django.db.models import Model, BinaryField, DateTimeField, TextField, ForeignKey, DO_NOTHING, SET_NULL, \
    OneToOneField, GenericIPAddressField, CASCADE
from django.utils.translation import gettext
from django_hash_field import HashField

from debts.internals import draw_digraph, WhoOwesWho, PaymentHistoryItem, PLOT_FILENAME_FORMAT, get_graph_integrity


class HashedFieldModelMixin(Model):
    hash_func = hashlib.sha3_256
    hashed = HashField(null=True, max_length=100)
    field_to_hash = None

    def get_hashed_value(self):
        return self.hash_func(self.get_value_to_hash()).hexdigest()

    def get_value_to_hash(self):
        return getattr(self, self.field_to_hash)

    def save(self, *args, **kwargs):
        self.hashed = self.get_hashed_value()
        return super(HashedFieldModelMixin, self).save(*args, **kwargs)

    def __str__(self):
        return self.hashed

    class Meta:
        abstract = True


class Graph(HashedFieldModelMixin, Model):
    field_to_hash = 'svg'

    svg = TextField(null=False, blank=False)

    def get_value_to_hash(self):  # we need to make it bytes since it's a string
        return bytes(super(Graph, self).get_value_to_hash(), encoding='UTF-8')


class BinaryFile(HashedFieldModelMixin, Model):
    field_to_hash = 'file_content'

    file_content = BinaryField()


class CalculationResult(Model):
    submitter = ForeignKey(User, on_delete=CASCADE)
    text_report = TextField()
    complex_graph = ForeignKey(Graph, on_delete=SET_NULL, null=True, related_name='complex_of')
    simple_graph = ForeignKey(Graph, on_delete=SET_NULL, null=True, related_name='simple_of')

    csv_file = ForeignKey(BinaryFile, on_delete=CASCADE, null=True)
    uploaded_at = DateTimeField(auto_now=True)

    def __str__(self):
        at = gettext('at')
        return f'{self.submitter} {at} {self.uploaded_at}'

    def as_dict(self):
        submitter_name = (self.submitter.first_name + ' ' + self.submitter.last_name) or self.submitter.username
        return {
            'submitter': submitter_name,
            'text_report': self.text_report,
            'complex_graph': self.complex_graph.svg,
            'simple_graph': self.simple_graph.svg,
            'uploaded_at': self.uploaded_at,
        }

    @classmethod
    def from_csv(cls, submitter: User, csv_content: StringIO, tmp_dir: str, full_names=False):
        output = StringIO()
        print_args = dict(
            file=output,
        )

        histories = PaymentHistoryItem.from_csv(fp=csv_content)
        all_people = PaymentHistoryItem.get_people_involved(histories)
        who_owes_who = PaymentHistoryItem.get_who_owes_who(histories, all_people)
        if len(who_owes_who) == 0:
            print(gettext('Nobody has any debts! Yay!'), **print_args)
            csv_file, _ = BinaryFile.objects.get_or_create(file_content=BytesIO(csv_content.getvalue().encode()).getvalue())
            csv_file.save()
            no_graph_message = Graph.objects.get_or_create(svg='No graphs for no debts. :)')
            return cls(
                submitter=submitter,
                text_report=output.getvalue(),
                complex_graph=no_graph_message,
                simple_graph=no_graph_message,
                csv_file=csv_file,
            )

        print(gettext('Summary') + ':', **print_args)
        for i, wow in enumerate(sorted(who_owes_who, key=lambda x: x.debt, reverse=True)):
            print(f'{i + 1:2}.', wow, **print_args)

        print(**print_args)

        complex_graph = PaymentHistoryItem.build_debt_graph(all_people, histories=histories, who_owes_who=who_owes_who)
        now = datetime.now()
        extension = 'svg'
        pre_filename = path.join(tmp_dir, PLOT_FILENAME_FORMAT.format(
            plot_name=f'{submitter.username}-pre',
            date=now,
            extension=extension,
        ))
        post_filename = path.join(tmp_dir, PLOT_FILENAME_FORMAT.format(
            plot_name=f'{submitter.username}-post',
            date=now,
            extension=extension,
        ))

        simple_graph = PaymentHistoryItem.minimize_debt_graph(complex_graph)

        svg_complex = draw_digraph(complex_graph, full_names, pre_filename, extension, return_file_content=True)
        svg_simplified = draw_digraph(simple_graph, full_names, post_filename, extension, return_file_content=True)
        integrity = get_graph_integrity(simple_graph)
        print(**print_args)

        new_who_owes_who = WhoOwesWho.from_debt_graph(simple_graph)
        print(gettext('Simplified summary') + ':', **print_args)
        for i, wow_n in enumerate(sorted(new_who_owes_who, key=lambda x: x.debt, reverse=True)):
            print(f'{i + 1:2}.', wow_n, **print_args)

        owed_to = {p: v for p, v in integrity.items() if v > 0}
        who_owes = {p: -v for p, v in integrity.items() if v < 0}

        is_owed = gettext('is owed')
        owes = gettext('owes')

        print(**print_args)
        print('-' * 80, **print_args)
        print(gettext('Cumulative debt:'), **print_args)
        for person, balance in owed_to.items():
            print('- {} {} {}'.format(person, is_owed, balance), **print_args)

        print(**print_args)

        for person, balance in who_owes.items():
            print('- {} {} {:.02f}'.format(person, owes, balance), **print_args)

        complex_graph_row, _ = Graph.objects.get_or_create(svg=svg_complex)
        simple_graph_row, _ = Graph.objects.get_or_create(svg=svg_simplified)
        csv_file, _ = BinaryFile.objects.get_or_create(file_content=BytesIO(csv_content.getvalue().encode()).getvalue())
        complex_graph_row.save()
        simple_graph_row.save()
        csv_file.save()

        return cls(
            submitter=submitter,
            text_report=output.getvalue(),
            complex_graph=complex_graph_row,
            simple_graph=simple_graph_row,
            csv_file=csv_file,
        )


class UserSettings(Model):
    user = OneToOneField(User, on_delete=CASCADE)
    timezone_name = TextField(choices=zip(pytz.common_timezones, pytz.common_timezones), default='UTC')

    def __str__(self):
        return str(self.user)


class IPTimezonePair(Model):
    ip = GenericIPAddressField(primary_key=True)
    timezone_name = TextField(choices=zip(pytz.common_timezones, pytz.common_timezones))

    def __str__(self):
        return self.ip
