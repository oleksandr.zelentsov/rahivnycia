from io import StringIO, BytesIO

import pytz
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views import View
from django.utils.translation import gettext

from debts.models import CalculationResult, UserSettings
from rah.settings import MEDIA_ROOT


def logout_view(request):
    logout(request)
    return redirect(reverse('index'))


class LoginView(View):
    template_name = 'login.html'

    def get(self, request):
        if request.user.is_authenticated:
            return redirect(reverse('index'))

        return render(request, self.template_name)

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        redir_url = request.GET.get('next', reverse('index'))
        user = authenticate(request, username=username, password=password)
        print(request.GET)
        if user is not None:
            login(request, user)
            return redirect(redir_url)
        else:
            return render(request, self.template_name, context=dict(errors=[gettext('invalid login info')]))


class IndexView(LoginRequiredMixin, View):
    login_url = reverse_lazy('user-login')
    template_name = 'index.html'

    def get(self, request):
        return render(request, self.template_name, context=dict(
            calculations=CalculationResult.objects.filter(submitter=request.user).order_by('-uploaded_at'),
        ))

    def post(self, request):
        csv_file = next((v for k, v in request.FILES.items() if v), None)
        csv_content = BytesIO()
        for chunk in csv_file.chunks():
            csv_content.write(chunk)

        csv_content = StringIO(csv_content.getvalue().decode())
        result = CalculationResult.from_csv(request.user, csv_content, MEDIA_ROOT)
        result.save()
        return redirect(reverse('user-view-results', kwargs=dict(result_id=result.id)))


class ResultDetailView(View):
    template_name = 'results.html'

    def get(self, request, result_id):
        try:
            result = CalculationResult.objects.get(pk=result_id)
        except CalculationResult.DoesNotExist:
            raise Http404()

        return render(request, self.template_name, context=result.as_dict())


class TimezoneSetting(LoginRequiredMixin, View):
    login_url = reverse_lazy('user-login')
    template_name = 'timezone_setting.html'

    def post(self, request):
        tz = request.session['django_timezone'] = request.POST['timezone']

        defaults = dict(
            timezone_name=tz,
        )
        UserSettings.objects.update_or_create(defaults=defaults, user=request.user)

        return redirect('/')

    def get(self, request):
        return render(request, self.template_name, {'timezones': pytz.common_timezones})
