import logging

import pytz

from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

from debts.internals import get_timezone_by_request, TimezoneLookupError
from debts.models import UserSettings


class TimezoneMiddleware(MiddlewareMixin):
    TIMEZONE_COOKIE_NAME = 'django_timezone'

    def process_request(self, request):
        # get timezone from cookies
        cookie_timezone = request.session.get(self.TIMEZONE_COOKIE_NAME)
        if cookie_timezone:  # if we got it, set it
            timezone.activate(pytz.timezone(cookie_timezone))
        else:  # if there is no cookie
            if request.user.is_authenticated:
                try:  # try to pull tz info from settings
                    settings_timezone = UserSettings.objects.get(user=request.user).timezone_name
                except UserSettings.DoesNotExist:
                    pass  # there are no settings for this user
                else:
                    self.set_session_timezone(settings_timezone, request)
                    return

            try:
                ip_time_zone = get_timezone_by_request(request)  # try to define it by IP
            except TimezoneLookupError as e:  # if nothing helped - just go with the default
                self.report_error_and_deactivate_timezone(e, 'timezone lookup error occurred')
            except Exception as e:
                self.report_error_and_deactivate_timezone(e, 'unknown kind of error occurred during the timezone lookup')
            else:
                if request.user.is_authenticated:
                    self.set_timezone_in_user_settings(ip_time_zone, request)

                self.set_session_timezone(ip_time_zone, request)

    @staticmethod
    def report_error_and_deactivate_timezone(e, log_msg):
        logging.error(log_msg)
        logging.exception(e)
        timezone.deactivate()

    @classmethod
    def set_session_timezone(cls, timezone_name, request):
        request.session[cls.TIMEZONE_COOKIE_NAME] = timezone_name
        timezone.activate(pytz.timezone(timezone_name))

    @staticmethod
    def set_timezone_in_user_settings(timezone_name, request):
        user_settings, created = UserSettings.objects.get_or_create(user=request.user)
        user_settings.timezone_name = timezone_name
        user_settings.save()
