# Generated by Django 2.0.6 on 2018-07-09 17:59
import hashlib

from django.db import migrations
import django_hash_field


def regenerate_hashes(apps, schema_editor):
    BinaryFile = apps.get_model('debts', 'BinaryFile')
    Graph = apps.get_model('debts', 'Graph')

    for graph in Graph.objects.all():
        graph.hashed = hashlib.sha3_256(bytes(graph.svg, encoding='UTF-8')).hexdigest()
        graph.save()

    for binfile in BinaryFile.objects.all():
        binfile.hashed = hashlib.sha3_256(binfile.file_content).hexdigest()
        binfile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('debts', '0006_auto_20180709_1759'),
    ]

    operations = [
        migrations.RunPython(regenerate_hashes),
    ]
