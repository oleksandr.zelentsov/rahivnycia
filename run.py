#!/usr/bin/env python3
"""
Use this to perform calculations about who owes how much to which people.
"""
import datetime
import os
from argparse import ArgumentParser, FileType

import django
from django.conf import settings

from debts.internals import WhoOwesWho, draw_digraph, PaymentHistoryItem, PLOT_FILENAME_FORMAT


def process_csv(args, parser):
    datafile = args.datafile
    histories = PaymentHistoryItem.from_csv(fp=datafile)
    all_people = PaymentHistoryItem.get_people_involved(histories)
    who_owes_who = PaymentHistoryItem.get_who_owes_who(histories, all_people)
    if len(who_owes_who) == 0:
        print('Nobody has any debts! Yay!')
        return

    print('Summary:')
    for i, wow in enumerate(sorted(who_owes_who, key=lambda x: x.debt, reverse=True)):
        print(f'{i + 1}.', wow)

    print()

    if args.graph:
        complex_graph = PaymentHistoryItem.build_debt_graph(all_people, histories=histories, who_owes_who=who_owes_who)
        if args.interactive_plots:
            pre_filename = post_filename = None
        else:
            now = datetime.datetime.now()
            pre_filename = os.path.join(args.output_path, PLOT_FILENAME_FORMAT.format(
                plot_name='pre',
                date=now,
                extension=args.plot_file_format,
            ))
            post_filename = os.path.join(args.output_path, PLOT_FILENAME_FORMAT.format(
                plot_name='post',
                date=now,
                extension=args.plot_file_format,
            ))

        simple_graph = PaymentHistoryItem.minimize_debt_graph(complex_graph)

        if not args.no_plots:
            draw_digraph(complex_graph, args.full_names, pre_filename, args.plot_file_format)
            draw_digraph(simple_graph, args.full_names, post_filename, args.plot_file_format)

        new_who_owes_who = WhoOwesWho.from_debt_graph(complex_graph)
        print('Simplified summary:')
        for i, wow in enumerate(sorted(new_who_owes_who, key=lambda x: x.debt, reverse=True)):
            print(f'{i + 1}.', wow)


def failure(args, parser):
    print('wrong arguments')
    parser.print_help()
    exit(1)


def get_args():
    parser = ArgumentParser(description='Get reports on who owes who.')
    parser.set_defaults(func=failure)
    subparsers = parser.add_subparsers(title='action')
    csv_parser = subparsers.add_parser('csv', help='get the data from CSV file')
    csv_parser.add_argument('datafile', type=FileType('r'), help='where to take the data')
    csv_parser.add_argument('-g', '--graph',
                            action='store_true',
                            default=False,
                            help='to resolve transitive debts using graphs')
    csv_parser.add_argument('-n', '--headless', '--no-plots', dest='no_plots',
                            action='store_true',
                            default=False,
                            help='to not show / save debt graphs')
    csv_parser.add_argument('-f', '--full-names',
                            action='store_true',
                            default=False,
                            help='to show initials on graphs')
    csv_parser.add_argument('-i', '--interactive-plots', dest='interactive_plots',
                            action='store_true',
                            default=False,
                            help='to show images in the interactive window instead of saving graphs to filesystem')
    csv_parser.add_argument('-p', '--output-path',
                            type=str,
                            default='.')
    csv_parser.add_argument('-e', '--plot-file-format', dest='plot_file_format',
                            type=str,
                            default='svg',
                            help='format of saved plot files',
                            choices=['png', 'svg', 'pdf'])
    csv_parser.set_defaults(func=process_csv)
    return parser.parse_args(), parser


def main():
    settings.configure()
    django.setup()
    args, parser = get_args()
    args.func(args, parser)


if __name__ == '__main__':
    main()
