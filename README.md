# Description
This app is for tracking personal debts, usually between friends.

# Getting started
## Installing dependencies
Some real basic stuff:
```bash
virtualenv -p python3.6 env && source env/bin/activate && python -m pip install -r requirements.txt
```

## Preparations to run as Django project

Run these commands to create a Django settings file. You can override some settings, like the database.
```bash
echo 'from rah.base_settings import *' > rah/settings.py
```

After that and `python manage.py migrate` you should be able to use the project.
It is advised to `python manage.py createsuperuser` to have an access to admin panel, because that's the only way to create other users.

## Running via cli
You can see everything the CLI can do by running:
```bash
./run.py --help
```