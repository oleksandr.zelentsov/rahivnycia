import hashlib
import random

import dj_database_url
import matplotlib

from rah.base_settings import *


USE_TZ = True

DATABASES = {'default': dj_database_url.config(default=os.environ["DATABASE_URL"])}


ALLOWED_HOSTS += [
    'rahivnycia.herokuapp.com',
    '0.0.0.0',
]

SECRET_KEY = os.environ['SECRET_KEY']

SESSION_COOKIE_AGE = 60 * 60

IP_STACK_ACCESS_KEY = os.environ['IP_STACK_ACCESS_KEY']

matplotlib.use('Agg')


