import unittest

from debts.internals import PaymentHistoryItem, get_graph_integrity
from rah_tests.helpers import generate_debt_graphs


class DebtGraphMinimizationTestCase(unittest.TestCase):
    def test_check_integrity_after_graph_minimize(self):
        for graph_complex in generate_debt_graphs():
            with self.subTest(g=graph_complex):
                graph_simplified = PaymentHistoryItem.minimize_debt_graph(graph_complex)
                complex_graph_integrity = get_graph_integrity(graph_complex)
                simplified_graph_integrity = get_graph_integrity(graph_simplified)
                self.assertDictEqual(complex_graph_integrity, simplified_graph_integrity)

    def test_minimized_graph_stays_the_same_after_minimization(self):
        for graph_complex in generate_debt_graphs():
            with self.subTest(g=graph_complex):
                graph_simplified1 = PaymentHistoryItem.minimize_debt_graph(graph_complex)
                graph_simplified2 = PaymentHistoryItem.minimize_debt_graph(graph_simplified1)
                gi1, gi2 = get_graph_integrity(graph_simplified1), get_graph_integrity(graph_simplified2)
                self.assertDictEqual(gi1, gi2)


if __name__ == '__main__':
    unittest.main()
