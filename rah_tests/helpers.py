import os
import random
from copy import deepcopy
from itertools import count, islice, combinations

import networkx as nx

from rah_tests.constants import TEST_GRAPHS_NUM, MAX_TEST_DEBT, TEST_PEOPLE, MAX_PEOPLE, TEST_GRAPH_EDGES


def all_people():
    for i in count(start=0, step=1):
        for p in TEST_PEOPLE:
            yield f'{p}{i}'


def get_test_csv_path(filename):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(dir_path, 'test_data', filename)


def get_graph_from_edges(edges):
    graph = nx.DiGraph()
    for v1, v2, debt in edges:
        graph.add_edge(v1, v2, debt=debt)
    return graph


def generate_debt_graph_edges(people_num=None, debts_num=None):
    generated_graph = []
    if people_num is None:
        min_people = 4
        max_people = MAX_PEOPLE
        people_num = random.randrange(min_people, max_people)

    people = list(islice(all_people(), 0, people_num))
    if debts_num is None:
        max_debts = len(list(combinations(people, 2)))
        min_debts = 3
        debts_num = random.randrange(min_debts, max_debts)

    while debts_num > 0:
        p1 = random.choice(people)
        p2 = random.choice(people)
        debt = random.choice(range(MAX_TEST_DEBT))
        edge = (p1, p2, debt)
        existing_edges = [(a, b) for a, b, _ in generated_graph]
        if (p1, p2) not in existing_edges and (p2, p1) not in existing_edges and p1 != p2:
            generated_graph.append(edge)
            debts_num -= 1
    return generated_graph


def generate_debt_graph(*args, **kwargs):
    return get_graph_from_edges(generate_debt_graph_edges(*args, **kwargs))


def generate_debt_graphs():
    graph_edges = deepcopy(TEST_GRAPH_EDGES)
    for _ in range(TEST_GRAPHS_NUM):
        generated_graph_edges = generate_debt_graph_edges()
        graph_edges.append(generated_graph_edges)

    random.shuffle(graph_edges)
    yield from map(get_graph_from_edges, graph_edges)
