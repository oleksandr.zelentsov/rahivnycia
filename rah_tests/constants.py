from string import ascii_uppercase

TEST_GRAPHS_NUM = 20
MAX_TEST_DEBT = 1000
TEST_PEOPLE = ascii_uppercase
MAX_PEOPLE = 50
TEST_GRAPH_EDGES = [
    [
        ('A', 'B', 20),
        ('B', 'C', 10),
        ('D', 'C', 20),
    ],
    [
        ('A', 'B', 20),
        ('B', 'C', 10),
        ('A', 'D', 5),
        ('D', 'C', 20),
    ],
    [
        ('A', 'B', 20),
        ('B', 'C', 10),
        ('A', 'C', 50),
        ('D', 'C', 20),
    ],
]